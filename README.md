# Example solution to the Dining Philosophers problem in Golang

![Dining philosophers execution example](dining-philosophers.svg "Dining Philosophers")

Implements a solution to the [Dining Philosophers Problem](https://en.wikipedia.org/wiki/Dining_philosophers_problem) by implementing a simple dining etiquette that only two non-adjacent diners can be eating simultaneously, and that all diners take turns around the table, thinking when they are not eating. Each philosopher is given random multipliers to determine how long they will take to eat and think. Each "fork" is represented as a mutex to ensure proper resource locking. An arbiter maintains synchronization between the diners to facilitate cooperation. This solution attempts to optimize for the fair distribution of resources (opportunities for eating & thinking).
