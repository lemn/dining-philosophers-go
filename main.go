package main

import (
	"flag"
	"fmt"
	"io"
	"math"
	"math/rand"
	"os"
	"os/signal"
	"sync"
	"time"

	"github.com/aquasecurity/table"
	"github.com/gosuri/uilive"
)

const (
	Waiting Activity = iota
	Eating
	Thinking
)

// Activity represents the different things a philospher can be doing
type Activity int

func (a Activity) String() string {
	switch a {
	case Waiting:
		return "Waiting"
	case Thinking:
		return "Thinking"
	case Eating:
		return "Eating"
	default:
		return ""
	}
}

var PhilosopherNames = []string{
	"Diogenes the Cynic",
	"Plato",
	"Aristotle",
	"Socrates",
	"Immanuel Kant",
	"René Descartes",
	"Michel Foucault",
	"Jean-Paul Sarte",
	"Ludwig Wittgenstein",
	"Friedrich Nietzsche",
	"Augustine of Hippo",
	"Karl Marx",
	"David Hume",
	"Thomas Aquinas",
	"Soren Kierkegaard",
	"Jacques Derrida",
	"Thomas Hobbes",
	"Gottfried Wilhelm Leibniz",
	"Blaise Pascal",
	"John Stuart Mill",
	"Voltaire",
	"Georg Wilhelm Friedrich Hegel",
	"Confucius",
	"Lao-Tzu",
	"John Locke",
	"Thomas Michael Scanlon",
	"Marcus Aurelius",
	"Epictetus",
	"Zeno of Citium",
	"Pythagorus",
	"Adam Smith",
	"Niccolò Machiavelli",
}

// GetName returns a single philospher, removing it from the original slice
func GetPhilosopher() string {
	idx := rand.Intn(len(PhilosopherNames))
	name := PhilosopherNames[idx]
	PhilosopherNames = append(PhilosopherNames[:idx], PhilosopherNames[idx+1:]...)
	return name
}

// Philosopher manages state and concurrency
type Philosopher struct {
	LeftFork     *sync.Mutex
	RightFork    *sync.Mutex
	LastEaten    time.Time
	LastThought  time.Time
	TimeEating   time.Duration
	TimeThinking time.Duration
	TimeWaiting  time.Duration
	TimesEaten   int
	TimesThought int
	TimesWaited  int
	MaxMult      float64
	Activity     Activity
	LastSet      time.Time
	Counter      int
	Position     int
	Name         string
}

// NewPhilosopher creates a Philosopher with defaults
func NewPhilosopher(left, right *sync.Mutex, pos int, max float64, name string) *Philosopher {
	p := &Philosopher{
		LeftFork:    left,
		RightFork:   right,
		LastEaten:   time.Unix(0, 0),
		LastThought: time.Unix(0, 0),
		MaxMult:     max, // maximum random eat/sleep multiplier
		Activity:    Waiting,
		LastSet:     time.Now(),
		Counter:     0,
		Position:    pos,
		Name:        name,
	}
	return p
}

// SetActivity sets the current activity of the philosopher and updates timers
func (p *Philosopher) SetActivity(a Activity) {
	prev := p.Activity
	switch prev {
	case Waiting:
		p.TimeWaiting = time.Since(p.LastSet) + p.TimeWaiting
		p.TimesWaited = p.TimesWaited + 1
	case Thinking:
		p.TimeThinking = time.Since(p.LastSet) + p.TimeThinking
		p.TimesThought = p.TimesThought + 1
	case Eating:
		p.TimeEating = time.Since(p.LastSet) + p.TimeEating
		p.TimesEaten = p.TimesEaten + 1
	}
	p.LastSet = time.Now()
	p.Activity = a
}

// Dine performs the dining action (eating or thinking, depending on circumstances)
func (p *Philosopher) Dine(ms time.Duration) {
	p.Counter = p.Counter + 1
	// only 2, non-adjacent philosophers can dine at a time
	// alternate using modulo 5 (number of diners)
	remainder := (p.Counter + p.Position) % 5
	if remainder == 0 || remainder == 3 {
		p.Eat(ms)
	} else {
		p.Think(ms)
	}
}

// Eat locks the required forks and spends time "eating", incrementing the duration spent
func (p *Philosopher) Eat(ms time.Duration) {
	// lock the forks/mutexes before eating
	// ensures that we're following the rules
	p.LeftFork.Lock()
	p.RightFork.Lock()

	// make sure to put down the forks/unlock
	// the mutexes when we're done eating
	defer p.LeftFork.Unlock()
	defer p.RightFork.Unlock()
	p.SetActivity(Eating)
	p.LastEaten = time.Now()
	sleepDur := time.Duration(float64(ms) * (1 + rand.Float64()*(p.MaxMult-1))) // min 1ms*interval
	time.Sleep(sleepDur)                                                        // take our time eating
	p.SetActivity(Waiting)
}

// Think spends time "thinking", incrementing the duration spent
func (p *Philosopher) Think(ms time.Duration) {
	p.SetActivity(Thinking)
	p.LastThought = time.Now()
	sleepDur := time.Duration(float64(ms) * (1 + rand.Float64()*(p.MaxMult-1))) // min 1ms*interval
	time.Sleep(sleepDur)                                                        // take our time thinking
	p.SetActivity(Waiting)
}

// RenderTable takes a writer object and philosophers and writes the formatted table output
func RenderTable(w io.Writer, ph ...*Philosopher) {
	t := table.New(w)
	t.SetHeaders("Position", "Name", "Activity", "Since Last Eaten", "Since Last Thought", "Time Eating", "Time Thinking", "Time Waiting", "Times Eaten", "Times Thought")
	t.SetLineStyle(table.StyleWhite)
	for _, p := range ph {
		t.AddRow(
			fmt.Sprint(p.Position),
			p.Name,
			fmt.Sprint(p.Activity),
			fmt.Sprint(time.Since(p.LastEaten)),
			fmt.Sprint(time.Since(p.LastThought)),
			fmt.Sprint(p.TimeEating),
			fmt.Sprint(p.TimeThinking),
			fmt.Sprint(p.TimeWaiting),
			fmt.Sprint(p.TimesEaten),
			fmt.Sprint(p.TimesThought),
		)
	}
	t.Render()
}

func GenerateSummary(ph ...*Philosopher) {
	var (
		totalEating   time.Duration
		totalThinking time.Duration
		totalWaiting  time.Duration
	)
	for _, p := range ph {
		totalEating = totalEating + p.TimeEating
		totalThinking = totalThinking + p.TimeThinking
		totalWaiting = totalWaiting + p.TimeWaiting
	}
	fmt.Println("Total time spent eating: ", totalEating)
	fmt.Println("Total time spent thinking: ", totalThinking)
	fmt.Println("Total time spent waiting: ", totalWaiting)
	fmt.Printf("Overall efficiency: %f %%", (float64((totalEating+totalThinking))/float64(totalEating+totalThinking+totalWaiting))*100)
}

func main() {

	// Setup flags
	numPhil := flag.Int("num", 5, "Number of philosophers")
	intTick := flag.Int("int", 500, "Length of base dining interval (in milliseconds)")
	maxMult := flag.Float64("max", 2, "Maximum dining time multiplier")
	// Parse flags
	flag.Parse()

	// forks each represented by a mutex, totalling one per philosopher
	var forks []sync.Mutex
	n := 0
	for n <= *numPhil {
		forks = append(forks, sync.Mutex{})
		n = n + 1
	}

	// set the table
	var philosophers []*Philosopher
	n = 0
	for n <= *numPhil {
		philosophers = append(philosophers, NewPhilosopher(&forks[n%*numPhil], &forks[(n+1)%*numPhil], n, *maxMult, GetPhilosopher()))
		n = n + 1
	}

	ms := time.Duration(*intTick) * time.Millisecond

	// instantiate our table view
	writer := uilive.New()
	refresh := time.Duration(math.Max(float64(ms/10), float64(10*time.Millisecond)))
	writer.RefreshInterval = refresh
	writer.Start()
	// clear the terminal
	fmt.Print("\033[H\033[2J")

	// use a wait group for synchronization instead
	var wg sync.WaitGroup
	go func() {
		for {
			for _, p := range philosophers {
				wg.Add(1)
				go func(p *Philosopher) {
					defer wg.Done()
					p.Dine(ms)
				}(p)
			}
			wg.Wait()
		}
	}()

	// setup our ticker for screen writing
	writeTicker := time.NewTicker(refresh)
	go func() {
		for {
			<-writeTicker.C
			RenderTable(writer, philosophers...)
		}
	}()

	// block & run until ctrl-c is received
	block := make(chan os.Signal, 1)
	signal.Notify(block, os.Interrupt)
	<-block
	writer.Stop()
	GenerateSummary(philosophers...)
	os.Exit(0)
}
