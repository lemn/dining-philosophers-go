module dining-philosophers-go

go 1.18

require (
	github.com/aquasecurity/table v1.5.1
	github.com/gosuri/uilive v0.0.4
)

require (
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
)
